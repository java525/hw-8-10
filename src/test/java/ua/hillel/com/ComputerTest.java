package ua.hillel.com;

import org.junit.jupiter.api.Test;
import ua.hillel.com.game.Computer;
import static org.junit.Assert.assertFalse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ComputerTest {

    String[] words = {"apple"};
    String userWord1 = "apple";
    String userWord2 = "apricot";

    Computer computer = new Computer();

    @Test
    public void shouldPickWork() {
        String pick = computer.pickWord(words);
        assertEquals(words[0], pick);
    }

    @Test
    public void shouldCheckIfUserIsRight()  {
        String pick = computer.pickWord(words);
        assertTrue( computer.checkIfUserIsRight(userWord1, pick));
        assertFalse( computer.checkIfUserIsRight(userWord2, pick));
    }
}
