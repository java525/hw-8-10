package ua.hillel.com.game;

import java.util.Arrays;
import java.util.Random;

public class Computer {

    private static final int quantityOfSecretDashes = 15;

    public String pickWord(String[] words) {
        int randomIndex = new Random().nextInt(words.length);
        String computerWord = words[randomIndex];
        return computerWord;
    }

    public boolean checkIfUserIsRight(String userWord, String computerWord) {
        if (computerWord.equals(userWord)) {
            return true;
        } else {
            showLetters(userWord, computerWord);
            return false;
        }
    }

    private void showLetters(String userWord, String computerWord) {
        char[] screen = new char[quantityOfSecretDashes];
        for (int i = 0; i <= screen.length - 1; i++) {
            screen[i] = '#';
        }
        int[] lengths = {userWord.length(), computerWord.length()};
        Arrays.sort(lengths);
        for (int i = 0; i < lengths[0]; i++) {
            if (computerWord.charAt(i) == userWord.charAt(i)) {
                screen[i] = computerWord.charAt(i);
            }
        }
        System.out.println(screen);
    }
}