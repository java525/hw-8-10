package ua.hillel.com.game;

import java.util.Scanner;

public class UserUi {

    Scanner scanner;

    public UserUi(Scanner scanner) {
        this.scanner = scanner;
    }

    public void greetings() {
        System.out.println("Hello! Welcome to the game.");
    }

    public String guessWord() {
        System.out.println("Guess the word");
        String userWord = scanner.nextLine();
        return userWord;
    }

    public void messageAboutGuess(boolean guess) {
        if (guess) {
            System.out.println("Congrats! You won!");
        } else {
            System.out.println("No-no-no. Try once again!");
        }
    }
}