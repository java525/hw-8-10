package ua.hillel.com.game;

import java.util.Scanner;

public class Service {

    String[] words;

    public Service(String[] words) {
        this.words = words;
    }

    UserUi ui = new UserUi(new Scanner(System.in));
    Computer computer = new Computer();

    public void run() {
        String computerWord = computer.pickWord(words);
        ui.greetings();
        boolean game = false;
        while (!game) {
            String userWord = ui.guessWord();
            game = computer.checkIfUserIsRight(userWord, computerWord);
            ui.messageAboutGuess(game);
            if (game) {
                break;
            }
        }
    }
}