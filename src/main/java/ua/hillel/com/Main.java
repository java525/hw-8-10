package ua.hillel.com;

public class Main {

    public static void main(String[] args) {
        String string = "aaabbbccd";
        System.out.println(findSymbolOccurance(string, 'a'));
        System.out.println(findSymbolOccurance(string, 'b'));
        System.out.println(findSymbolOccurance(string, 'c'));
        System.out.println(findSymbolOccurance(string, 'd'));

        String source1 = "Apollo";
        String target1 = "pollo";
        System.out.println(findWordPosition(source1, target1));

        String source2 = "Apple";
        String target2 = "Plant";
        System.out.println(findWordPosition(source2, target2));

        String test3 = "Hellohello";
        System.out.println(stringReverse(test3));
    }

    private static int findSymbolOccurance(String string, char character) {
        int count = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == character) {
                count++;
            }
        }
        return count;
    }

    private static int findWordPosition(String source, String target) {
        return source.indexOf(target);
    }

    private static String stringReverse(String string) {
        char[] newArray = new char[string.length()];
        for (int i = 0; i < string.length(); i++) {
            newArray[i] = string.charAt(string.length() - i - 1);
        }
        String newString = String.valueOf(newArray);
        return newString;
    }
}